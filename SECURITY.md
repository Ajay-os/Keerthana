# Security Policy

## Supported Versions

| Version | Supported          |
| ------- | ------------------ |
| 5.1.0   | :privacy added : |
| 5.1.x   | :user contol:                |
| 5.2.0   | :user setup plugins |

## Reporting a Vulnerability

Use [issues](https://github.com/Ajayos/Keerthana/issues) section or contact to [mail](mailto:ajayosakhub@gmail.com) to report a vulnerability.
