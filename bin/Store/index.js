"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeInMemoryStore = void 0;
const store_1 = __importDefault(require("./store"));
exports.makeInMemoryStore = store_1.default;
